

module.exports = [
  {
    url: '/vue-admin-template/goods/cate',
    type: 'get',
    response: config => {
      const { username } = config.body
      const token = tokens[username]

      // mock error
      if (!token) {
        return {
          code: 60204,
          message: 'Account and password are incorrect.'
        }
      }

      return {
        code: 20000,
        data: [{
            "id": 9,
            "pid": 0,
            "name": "分类3",
            "image": "",
            "level": 1,
            "sort": 0,
            "status": 1,
            "goods_num": 0,
            "is_hidden": 0,
            "create_time": "2021-12-11 16:22:01",
            "update_time": "2021-12-11 16:22:01",
            "delete_time": null,
            "status_name": "启用",
            "level_name": "一级分类",
            "father_name": "",
            "children": []
          },
          {
            "id": 8,
            "pid": 0,
            "name": "分类2",
            "image": "",
            "level": 1,
            "sort": 0,
            "status": 1,
            "goods_num": 0,
            "is_hidden": 0,
            "create_time": "2021-12-11 16:21:51",
            "update_time": "2021-12-11 16:21:51",
            "delete_time": null,
            "status_name": "启用",
            "level_name": "一级分类",
            "father_name": "",
            "children": []
          },
          {
            "id": 7,
            "pid": 0,
            "name": "分类1",
            "image": "",
            "level": 1,
            "sort": 0,
            "status": 1,
            "goods_num": 1,
            "is_hidden": 0,
            "create_time": "2021-12-11 16:21:39",
            "update_time": "2021-12-11 16:21:39",
            "delete_time": null,
            "status_name": "启用",
            "level_name": "一级分类",
            "father_name": "",
            "children": []
          }]
      }
    }
  },
]
