import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/config/index_menu',
    method: 'get',
    params:data,
  })
}

export function updateIndexMenu(data) {
  return request({
    url: '/admin/config/index_menu/' + data.id,
    method: 'post',
    data
  })
}

export function createIndexMenu(data) {
  return request({
    url: '/admin/config/index_menu',
    method: 'post',
    data
  })
}

export function deleteIndexMenu(data) {
  return request({
    url: '/admin/config/index_menu/' + data.id,
    method: 'delete'
  })
}

export function detailIndexMenu(id) {
  return request({
    url: '/admin/config/index_menu/' + id,
    method: 'get'
  })
}
