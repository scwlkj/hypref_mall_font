import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/config/hot_search',
    method: 'get',
    params:data,
  })
}

export function updateHotSearch(data) {
  return request({
    url: '/admin/config/hot_search/' + data.id,
    method: 'post',
    data
  })
}

export function createHotSearch(data) {
  return request({
    url: '/admin/config/hot_search',
    method: 'post',
    data
  })
}

export function deleteHotSearch(data) {
  return request({
    url: '/admin/config/hot_search/' + data.id,
    method: 'delete'
  })
}

export function detailHotSearch(id) {
  return request({
    url: '/admin/config/hot_search/' + id,
    method: 'get'
  })
}
