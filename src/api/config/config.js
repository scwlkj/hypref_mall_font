import request from '@/utils/request'

export function getConfig(key) {
  return request({
    url: '/admin/config/config/'+key,
    method: 'get'
  })
}

export function addOrUpdateConfig(data) {
  return request({
    url: '/admin/config/config/set',
    method: 'post',
    data
  })
}
