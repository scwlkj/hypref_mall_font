import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/goods/brand',
    method: 'get',
    params:data,
  })
}

export function updateBrand(data) {
  return request({
    url: '/admin/goods/brand/' + data.id,
    method: 'post',
    data
  })
}

export function createBrand(data) {
  return request({
    url: '/admin/goods/brand',
    method: 'post',
    data
  })
}

export function deleteBrand(data) {
  return request({
    url: '/admin/goods/brand/' + data.id,
    method: 'delete'
  })
}
