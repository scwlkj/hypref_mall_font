import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/goods/tag',
    method: 'get',
    params:data,
  })
}

export function updateTag(data) {
  return request({
    url: '/admin/goods/tag/' + data.id,
    method: 'post',
    data
  })
}

export function createTag(data) {
  return request({
    url: '/admin/goods/tag',
    method: 'post',
    data
  })
}

export function deleteTag(data) {
  return request({
    url: '/admin/goods/tag/' + data.id,
    method: 'delete'
  })
}
