import request from '@/utils/request_image'

export function fetchList(data) {
  return request({
    url: '/index/list',
    method: 'get',
    params:data,
  })
}

export function addFolder(data) {
  return request({
    url: '/index/addFolder',
    method: 'post',
    data,
  })
}

export function folderList() {
  return request({
    url: '/index/folderList',
    method: 'get',
  })
}

export function moveFolder(data) {
  return request({
    url: '/index/moveFolder',
    method: 'post',
    data,
  })
}