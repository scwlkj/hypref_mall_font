import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/goods/goods',
    method: 'get',
    params:data,
  })
}

export function updateGoods(data, id) {
  return request({
    url: '/admin/goods/goods/'+ id,
    method: 'post',
    data
  })
}

export function createIndex(data) {
  return request({
    url: '/admin/goods/goods',
    method: 'post',
    data
  })
}

export function detail(id) {
  return request({
     url: '/admin/goods/goods/' + id,
    method: 'get'
  })
}

export function deleteIndex(data) {
  return request({
    url: '/admin/goods/goods/' + data.id,
    method: 'delete'
  })
}

export function init() {
  return request({
    url: '/admin/goods/goods/init',
    method: 'get'
  })
}
