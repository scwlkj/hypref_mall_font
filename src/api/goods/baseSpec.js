import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/goods/base_spec',
    method: 'get',
    params:data,
  })
}

export function updateBaseSpec(data) {
  return request({
    url: '/admin/goods/base_spec/' + data.id,
    method: 'post',
    data
  })
}

export function createBaseSpec(data) {
  return request({
    url: '/admin/goods/base_spec',
    method: 'post',
    data
  })
}

export function deleteBaseSpec(data) {
  return request({
    url: '/admin/goods/base_spec/' + data.id,
    method: 'delete'
  })
}

export function detailBaseSpec(id) {
  return request({
    url: '/admin/goods/base_spec/' + id,
    method: 'get'
  })
}
