import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/goods/base_attr',
    method: 'get',
    params:data,
  })
}

export function updateBaseAttr(data) {
  return request({
    url: '/admin/goods/base_attr/' + data.id,
    method: 'post',
    data
  })
}

export function createBaseAttr(data) {
  return request({
    url: '/admin/goods/base_attr',
    method: 'post',
    data
  })
}

export function deleteBaseAttr(data) {
  return request({
    url: '/admin/goods/base_attr/' + data.id,
    method: 'delete'
  })
}

export function detailBaseAttr(id) {
  return request({
    url: '/admin/goods/base_attr/' + id,
    method: 'get'
  })
}
