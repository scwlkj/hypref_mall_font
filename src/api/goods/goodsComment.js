import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/order/comment/list',
    method: 'get',
    params:data,
  })
}

export function pass(data) {
  return request({
    url: '/admin/order/comment/pass',
    method: 'post',
    data,
  })
}

export function unpass(data) {
  return request({
    url: '/admin/order/comment/unPass',
    method: 'post',
    data,
  })
}

export function replay(data) {
  return request({
    url: '/admin/order/comment/replay',
    method: 'post',
    data,
  })
}