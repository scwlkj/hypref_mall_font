import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/activity/coupon/coupon',
    method: 'get',
    params:data,
  })
}


export function detail(id) {
  return request({
     url: '/admin/activity/coupon/coupon/' + id,
    method: 'get'
  })
}

export function updateStatus(data) {
    return request({
        url: '/admin/activity/coupon/coupon/updateStatus',
        method: 'post',
        data,
    })
}
export function createcoupon(data) {
    return request({
        url: '/admin/activity/coupon/coupon/create',
        method: 'post',
        data,
    })
}

export function receiveList(data) {
    return request({
        url: '/admin/activity/coupon/coupon/receiveList',
        method: 'get',
        params:data,
    })
}
