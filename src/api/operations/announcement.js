import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/config/notify',
    method: 'get',
    params:data,
  })
}

export function updateNotify(data) {
  return request({
    url: '/admin/config/notify/' + data.id,
    method: 'post',
    data
  })
}

export function createNotify(data) {
  return request({
    url: '/admin/config/notify',
    method: 'post',
    data
  })
}

export function deleteNotify(data) {
  return request({
    url: '/admin/config/notify/' + data.id,
    method: 'delete'
  })
}

export function detailNotify(id) {
  return request({
    url: '/admin/config/notify/' + id,
    method: 'get'
  })
}
