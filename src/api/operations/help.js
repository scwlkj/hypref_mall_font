import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/config/article',
    method: 'get',
    params:data,
  })
}

export function updateHelp(data) {
  return request({
    url: '/admin/config/article/' + data.id,
    method: 'post',
    data
  })
}

export function createHelp(data) {
  return request({
    url: '/admin/config/article',
    method: 'post',
    data
  })
}

export function deleteHelp(data) {
  return request({
    url: '/admin/config/article/' + data.id,
    method: 'delete'
  })
}

export function detailHelp(id) {
  return request({
    url: '/admin/config/article/' + id,
    method: 'get'
  })
}
