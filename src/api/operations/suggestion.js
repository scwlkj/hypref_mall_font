import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/config/suggestion',
    method: 'get',
    params:data,
  })
}

export function updateSuggestion(data) {
  return request({
    url: '/admin/config/suggestion/' + data.id,
    method: 'post',
    data
  })
}


export function deleteSuggestion(data) {
  return request({
    url: '/admin/config/suggestion/' + data.id,
    method: 'delete'
  })
}


