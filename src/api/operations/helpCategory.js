import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/config/article_category/tree',
    method: 'get',
    params:data,
  })
}

export function selectTree() {
  return request({
    url: '/admin/config/article_category/selectTree',
    method: 'get',
  })
}

export function updateHelpCategory(data) {
  return request({
    url: '/admin/config/article_category/' + data.id,
    method: 'post',
    data
  })
}

export function createHelpCategory(data) {
  return request({
    url: '/admin/config/article_category',
    method: 'post',
    data
  })
}

export function deleteHelpCategory(data) {
  return request({
    url: '/admin/config/article_category/' + data.id,
    method: 'delete'
  })
}

export function detailHelpCategory(id) {
  return request({
    url: '/admin/config/article_category/' + id,
    method: 'get'
  })
}
