import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/config/adv',
    method: 'get',
    params:data,
  })
}

export function updateAdv(data) {
  return request({
    url: '/admin/config/adv/' + data.id,
    method: 'post',
    data
  })
}

export function createAdv(data) {
  return request({
    url: '/admin/config/adv',
    method: 'post',
    data
  })
}

export function deleteAdv(data) {
  return request({
    url: '/admin/config/adv/' + data.id,
    method: 'delete'
  })
}

export function detailAdv(id) {
  return request({
    url: '/admin/config/adv/' + id,
    method: 'get'
  })
}

export function getPosition() {
  return request({
    url: '/admin/config/adv/position',
    method: 'get'
  })
}

