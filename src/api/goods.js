import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/goods/category/tree',
    method: 'get',
    params:data,
  })
}

export function selectTree() {
  return request({
    url: '/admin/goods/category/selectTree',
    method: 'get',
  })
}

export function updateCate(data) {
  return request({
    url: '/admin/goods/category/' + data.id,
    method: 'post',
    data
  })
}

export function createCate(data) {
  return request({
    url: '/admin/goods/category',
    method: 'post',
    data
  })
}

export function deleteCate(data) {
  return request({
    url: '/admin/goods/category/' + data.id,
    method: 'delete'
  })
}
