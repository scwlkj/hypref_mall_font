import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/member/member',
    method: 'get',
    params:data,
  })
}


export function detail(id) {
  return request({
     url: '/admin/member/' + id,
    method: 'get'
  })
}
