import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/member/levels',
    method: 'get',
    params:data,
  })
}


export function detail(id) {
  return request({
     url: '/admin/member/levels/' + id,
    method: 'get'
  })
}

export function updateLevel(data) {
  return request({
    url: '/admin/member/levels/' + data.id,
    method: 'post',
    data
  })
}

export function createLevel(data) {
  return request({
    url: '/admin/member/levels',
    method: 'post',
    data
  })
}

export function deleteLevel(data) {
  return request({
    url: '/admin/member/levels/' + data.id,
    method: 'delete'
  })
}

