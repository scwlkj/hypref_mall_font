import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/order/order',
    method: 'get',
    params:data,
  })
}

export function orderDetail(data) {
  return request({
    url: '/admin/order/order/info',
    method: 'get',
    params:data,
  })
}

export function cancelOrder(data) {
  return request({
    url: '/admin/order/order/cancel',
    method: 'post',
    data,
  })
}

export function payedOrder(data) {
  return request({
    url: '/admin/order/order/payed',
    method: 'post',
    data,
  })
}

export function express() {
  return request({
    url: '/admin/config/express/list',
    method: 'get',
  })
}

export function delivery(data) {
  return request({
    url: '/admin/order/order/delivery',
    method: 'post',
    data,
  })
}

export function confirmGoods(data) {
  return request({
    url: '/admin/order/order/confirm_goods',
    method: 'post',
    data,
  })
}