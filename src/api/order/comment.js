import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/order/comment/orderList',
    method: 'get',
    params:data,
  })
}
