import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/admin/order/after/list',
    method: 'get',
    params:data,
  })
}

export function agree(data) {
  return request({
    url: '/admin/order/after/agree',
    method: 'post',
    data,
  })
}


export function refuse(data) {
  return request({
    url: '/admin/order/after/refuse',
    method: 'post',
    data,
  })
}


export function detail(data) {
  return request({
    url: '/admin/order/after/info',
    method: 'get',
    params:data,
  })
}