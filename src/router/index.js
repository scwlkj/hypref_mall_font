import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '系统面板', icon: 'dashboard'}
    }]
  },
  {
    path: '/goods',
    component: Layout,
    name: 'goods',
    meta: {
      title: '商品管理',
      icon: 'el-icon-goods'
    },
    redirect: './goodsList', // Parent router-view
    children: [
      {
        path: 'menu1',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'goodsScan',
        hidden:true,
        meta: { title: '商品概览' },
      },
      {
        path: 'goodsList',
        component: () => import('@/views/goods/goods'), // Parent router-view
        name: 'goodsList',
        meta: { title: '商品列表' },
      },

      {
        path: 'createGoods',
        component: () => import('@/views/form/goods/goods'), // Parent router-view
        name: 'createGoods',
        hidden: true,
        meta: { title: '新建商品' },
      },
      {
        path: 'updateGoods/:id',
        component: () => import('@/views/form/goods/goods'), // Parent router-view
        name: 'updateGoods',
        hidden: true,
        meta: { title: '编辑商品' },
      },
      {
        path: 'goodsCate',
        component: () => import('@/views/goods/goodsCate'),
        name: 'goodsCate',
        meta: { title: '商品分类' }
      },
      {
        path: 'goodsBrand',
        component: () => import('@/views/goods/goodsBrand'),
        name: 'goodsBrand',
       // hidden: true,
        meta: { title: '商品品牌' }
      },
      {
        path: 'baseSpec',
        component: () => import('@/views/goods/baseSpec'), // Parent router-view
        name: 'baseSpec',
        meta: { title: '商品规格' },
      },
      {
        path: 'createBaseSpec',
        name: 'createBaseSpec',
        component: () => import('@/views/form/baseSpec'),
        hidden: true,
        meta: { title: '新建规格' },
      },
      {
        path: 'baseAttr',
        component: () => import('@/views/goods/baseAttr'), // Parent router-view
        name: 'baseAttr',
        meta: { title: '商品类型' },
      },
      {
        path: 'createBaseAttr',
        name: 'createBaseAttr',
        component: () => import('@/views/form/baseAttr'),
        hidden: true,
        meta: { title: '新建类型' },
      },
      {
        path: 'goodsTag',
        component: () => import('@/views/goods/goodsTag'), // Parent router-view
        name: 'goodsTag',
        meta: { title: '商品标签' },
      },
      {
        path: 'goodsComment',
        component: () => import('@/views/goods/goodsComment'), // Parent router-view
        name: 'goodsComment',
        meta: { title: '商品评论' },
      }
    ]
  },

  {
    path: '/order',
    component: Layout,
    name: 'order',
    meta: {
      title: '订单管理',
      icon: 'el-icon-s-order'
    },
    redirect: './order', // Parent router-view
    children: [
      {
        path: 'orderScan',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'orderScan',
        hidden:true,
        meta: { title: '订单概览' },
      },
      {
        path: 'logisticsOrder',
        component: () => import('@/views/order/order'), // Parent router-view
        name: 'logisticsOrder',
        meta: { title: '物流订单' },
      },
      {
        path: 'orderDetail',
        component: () => import('@/views/order/detail'), // Parent router-view
        name: 'orderDetail',
        hidden:true,
        meta: { title: '订单详情' },
      },
      {
        path: 'orderShip',
        component: () => import('@/views/order/ship'), // Parent router-view
        name: 'orderShip',
        hidden:true,
        meta: { title: '订单发货' },
      },
      {
        path: 'selfTakeupOrder',
        component: () => import('@/views/nested/menu2/index'),
        name: 'selfTakeupOrder',
        hidden:true,
        meta: { title: '自提订单' }
      },
      {
        path: 'shipmentsOrder',
        component: () => import('@/views/order/shipOrder'),
        name: 'shipmentsOrder',
        meta: { title: '发货记录' }
      },
      {
        path: 'saleAfter',
        component: () => import('@/views/order/after'),
        name: 'saleAfter',
        meta: { title: '售后管理' }
      },
      {
        path: 'afterDetail',
        component: () => import('@/views/order/afterDetail'),
        name: 'afterDetail',
        hidden:true,
        meta: { title: '售后详情' }
      },
      {
        path: 'orderLog',
        component: () => import('@/views/nested/menu2/index'),
        name: 'orderLog',
        hidden:true,
        meta: { title: '订单记录' }
      },
      {
        path: 'comment',
        component: () => import('@/views/order/comment'), // Parent router-view
        name: 'comment',
        meta: { title: '订单评论' },
      }
    ]
  },

  {
    path: '/member',
    component: Layout,
    name: 'member',
    meta: {
      title: '会员管理',
      icon: 'el-icon-s-custom'
    },
    redirect: './member', 
    children: [
      {
        path: 'memberScan',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'memberScan',
        hidden:true,
        meta: { title: '会员概览' },
      },
      {
        path: 'memberList',
        component: () => import('@/views/member/member'), // Parent router-view
        name: 'memberList',
        meta: { title: '会员列表' },
      },

      {
        path: 'addLevel',
        component: () => import('@/views/form/member/levels'),
        name: 'addLevel',
        hidden: true,
        meta: { title: '添加会员等级' },
      },
      {
        path: 'memberLevel',
        component: () => import('@/views/member/memberLevel'),
        name: 'memberLevel',
        meta: { title: '会员等级' }
      }
    ]
  },

  // {
  //   path: '/shop',
  //   component: Layout,
  //   name: 'shop',
  //   meta: {
  //     title: '店铺管理',
  //     icon: 'el-icon-s-shop'
  //   },
  //   children: [
  //     {
  //       path: 'logisticsTemplate',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'logisticsTemplate',
  //       meta: { title: '物流配送' },
  //     }
  //   ]
  // },
  {
    path: '/activity',
    component: Layout,
    name: 'activity',
    meta: {
      title: '营销管理',
      icon: 'el-icon-s-flag'
    },
    redirect: './coupon', 
    children: [
      {
        path: 'coupon',
        component: () => import('@/views/activity/coupon/index'), // Parent router-view
        name: 'coupon',
        meta: { title: '优 惠 券' },
      },

      {
        path: 'addCoupon',
        component: () => import('@/views/form/activity/coupon/coupon'), // Parent router-view
        name: 'addCoupon',
        hidden: true,
        meta: { title: '添加优惠券' },
      },
     
      {
        path: 'group',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'group',
       // hidden: true,
        meta: { title: '团购活动' },
      }
    ]
  },
  // {
  //   path: '/point',
  //   component: Layout,
  //   name: 'point',
  //   meta: {
  //     title: '积分商城',
  //     icon: 'el-icon-s-claim'
  //   },
  //   children: [
  //     {
  //       path: 'pointGoods',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'pointGoods',
  //       meta: { title: '积分商品' },
  //     },
  //     {
  //       path: 'pointCate',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'pointCate',
  //       meta: { title: '商品分类' },
  //     },
  //     {
  //       path: 'pointOrder',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'pointOrder',
  //       meta: { title: '积分订单' },
  //     }
  //   ]
  // },
  // {
  //   path: '/financial',
  //   component: Layout,
  //   name: 'financial',
  //   meta: {
  //     title: '财务管理',
  //     icon: 'el-icon-s-finance'
  //   },
  //   children: [
  //     {
  //       path: 'flowDetail',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'flowDetail',
  //       meta: { title: '流水明细' },
  //     },
  //     {
  //       path: 'drawLog',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'drawLog',
  //       meta: { title: '提现记录' },
  //     },
  //     {
  //       path: 'drawConfig',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'drawConfig',
  //       meta: { title: '提现配置' },
  //     }
  //   ]
  // },
  {
    path: '/operations',
    component: Layout,
    name: 'operations',
    meta: {
      title: '运营管理',
      icon: 'el-icon-s-management'
    },
    redirect: './notify', 
    children: [
      {
        path: 'announcement',
        component: () => import('@/views/operations/notify'), // Parent router-view
        name: 'announcement',
        meta: { title: '公告管理' },
      },
      {
        path: 'createNotify',
        component: () => import('@/views/form/operations/notify'), // Parent router-view
        name: 'createNotify',
        hidden: true,
        meta: { title: '添加公告' },
      },
      {
        path: 'suggestion',
        component: () => import('@/views/operations/suggestion'), // Parent router-view
        name: 'suggestion',
        meta: { title: '建议反馈' },
      },
      {
        path: 'helpCategory',
        component: () => import('@/views/operations/helpCategory'), // Parent router-view
        name: 'helpCategory',
        meta: { title: '帮助分类' },
      },

      {
        path: 'createHelpCategory',
        component: () => import('@/views/form/operations/helpCategory'), // Parent router-view
        name: 'createHelpCategory',
        hidden: true,
        meta: { title: '创建分类' },
      },
      {
        path: 'help',
        component: () => import('@/views/operations/help'), // Parent router-view
        name: 'help',
        meta: { title: '站点帮助' },
      },
      {
        path: 'createHelp',
        component: () => import('@/views/form/operations/help'), // Parent router-view
        name: 'createHelp',
        hidden: true,
        meta: { title: '添加帮助' },
      },
      {
        path: 'adv',
        component: () => import('@/views/operations/adv'), // Parent router-view
        name: 'adv',
        meta: { title: '广告管理' },
      },
      {
        path: 'createadv',
        component: () => import('@/views/form/operations/adv'), // Parent router-view
        name: 'createadv',
        hidden: true,
        meta: { title: '广告管理' },
      },
      {
        path: 'copyRight',
        component: () => import('@/views/form/operations/copyRight'), // Parent router-view
        name: 'copyRight',
        meta: { title: '版权信息' },
      },
    ]
  },
  {
    path: '/system',
    component: Layout,
    name: 'system',
    meta: {
      title: '系统配置',
      icon: 'el-icon-s-management'
    },
    redirect: './hotSearch', 
    children: [
       {
        path: 'hotSearch',
        component: () => import('@/views/config/hotSearch'), // Parent router-view
        name: 'hotSearch',
        meta: { title: '热搜词' },
      },
      {
        path: 'createHotSearch',
        component: () => import('@/views/form/config/hotSearch'), // Parent router-view
        name: 'createHotSearch',
        hidden:true,
        meta: { title: '添加热搜词' },
      },
      {
        path: 'indexMenu',
        component: () => import('@/views/config/indexMenu'), // Parent router-view
        name: 'indexMenu',
        meta: { title: '首页菜单配置' },
      },
      {
        path: 'createIndexMenu',
        component: () => import('@/views/form/config/indexMenu'), // Parent router-view
        name: 'createIndexMenu',
        hidden:true,
        meta: { title: '创建首页菜单' },
      },
      {
        path: 'setting',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'setting',
        meta: { title: '账户设置' },
        children: [
          {
            path: 'user',
            component: () => import('@/views/nested/menu1/index'), // Parent router-view
            name: 'user',
            meta: { title: '账户管理' },
          },
          {
            path: 'role',
            component: () => import('@/views/nested/menu1/index'), // Parent router-view
            name: 'role',
            meta: { title: '角色管理' },
          },
          {
            path: 'loginLog',
            component: () => import('@/views/nested/menu1/index'), // Parent router-view
            name: 'loginLog',
            meta: { title: '登录日志' },
          },
          {
            path: 'oparateLog',
            component: () => import('@/views/nested/menu1/index'), // Parent router-view
            name: 'oparateLog',
            meta: { title: '操作记录' },
          }
        ]
      },
      {
        path: 'pay',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'pay',
        meta: { title: '支付配置' },
      },
    ]
  },
  /*
  {
    path: '/example',
    component: Layout,
    redirect: '/example/table',
    name: 'Example',
    meta: { title: 'Example', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/table/index'),
        meta: { title: 'Table', icon: 'table' }
      },
      {
        path: 'tree',
        name: 'Tree',
        component: () => import('@/views/tree/index'),
        meta: { title: 'Tree', icon: 'tree' }
      }
    ]
  },

  {
    path: '/form',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Form',
        component: () => import('@/views/form/index'),
        meta: { title: 'Form', icon: 'form' }
      }
    ]
  },
  */

  /* {
    path: '/nested',
    component: Layout,
    redirect: '/nested/menu1',
    name: 'Nested',
    meta: {
      title: 'Nested',
      icon: 'nested'
    },
    children: [
      {
        path: 'menu1',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'Menu1',
        meta: { title: 'Menu1' },
        children: [
          {
            path: 'menu1-1',
            component: () => import('@/views/nested/menu1/menu1-1'),
            name: 'Menu1-1',
            meta: { title: 'Menu1-1' }
          },
          {
            path: 'menu1-2',
            component: () => import('@/views/nested/menu1/menu1-2'),
            name: 'Menu1-2',
            meta: { title: 'Menu1-2' },
            children: [
              {
                path: 'menu1-2-1',
                component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
                name: 'Menu1-2-1',
                meta: { title: 'Menu1-2-1' }
              },
              {
                path: 'menu1-2-2',
                component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
                name: 'Menu1-2-2',
                meta: { title: 'Menu1-2-2' }
              }
            ]
          },
          {
            path: 'menu1-3',
            component: () => import('@/views/nested/menu1/menu1-3'),
            name: 'Menu1-3',
            meta: { title: 'Menu1-3' }
          }
        ]
      },
      {
        path: 'menu2',
        component: () => import('@/views/nested/menu2/index'),
        name: 'Menu2',
        meta: { title: 'menu2' }
      }
    ]
  },

  {
    path: 'external-link',
    component: Layout,
    children: [
      {
        path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
        meta: { title: 'External Link', icon: 'link' }
      }
    ]
  }, */

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
