# hyperf_mall_font

> hyperf_mall 管理后台，基于vue_admin_template。

[线上地址](https://gitee.com/scwlkj/hyperf_mall_font)

[hyperf_mall 安装地址](https://gitee.com/scwlkj/hyperf_mall)


## hyperf_mall一键部署实现步骤

```bash
# 克隆项目
git clone https://gitee.com/scwlkj/hyperf_mall_font

# 进入项目目录
cd hypref_mall_font

# 安装依赖
npm install --registry=https://registry.npm.taobao.org

# 构建html代码
npm run build:prod

# 构建Docker镜像 ,使用项目下的Dockerfile文件
docker build -t hypref_mall_font .

# 启动服务 --name为服务名 后面的hypref_mall_font为镜像
docker run --name  hyperf_mall_font -d -p 5569:80 hypref_mall_font
```

浏览器访问 [http://localhost:5569](http://localhost:5569)



## hypref_mall 维护命令实现步骤

```bash
# 进入项目目录
cd hypref_mall_font

# 拉取代码
git  pull

# 安装新的依赖
npm install --registry=https://registry.npm.taobao.org

# 构建html代码
npm run build:prod

# 构建新的Docker镜像 ,使用项目下的Dockerfile文件
docker build -t hyperf_mall_font .

# 删除原有的容器
docker rm -f hyperf_mall_font

# 启动服务 --name为服务名 后面的hypref_mall_font为镜像
docker run --name  hyperf_mall_font -d -p 5569:80 hyperf_mall_font
```